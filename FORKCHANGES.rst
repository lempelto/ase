Changes made by Aku Lempelto
============================

- Menus can no longer be detached from the window.
  **ui.py, class MainWindow**::
    
    self.win.option_add('\*tearoff', tk.FALSE)

  This is simply a stylistic choice but it makes the look and feel more
  contemporary.


- | A 'Preferences' menu has been added (**preferences.py**)
  | The Preferences menu effectively acts as an of interface to the 
    ``gui.config`` dictionary. All newly added preferences are also
    added to gui.config which, in my opinion, serves the purpose well.
  | Currently the code creates a file under the user's home
    directory: ``~/.config/ase/gui.ini``
    This is where the settings are saved to and fetched from. Going
    forward, it may be good to give the user a way to change this eg.
    when ASE is installed.


- An undo feature has been implemented using four new methods in the
  GUI class in **gui.py**: ::

    init_history()
    update_history()
    undo_history()
    redo_history()

  - Call ``init_history()`` to initialise history. Creates the list
    ``gui.history`` containing a deque object (list-like) for each
    image which will hold past (and future) versions of each image.
    Also creates the list ``gui.hnow`` containing an index (``int``) of
    where in history each image currently is. All images opened
    therefore have their own, independent histories.
  - ``update_history()`` takes a "snapshot" of the current state of the
    active frame and adds it to the history of the corresponding image.
  - ``undo_history()`` gets the previous state of the current frame from 
    ``gui.history`` (if available) and sets it as visible.
  - ``redo_history()`` is the same but forwards.


- Window height and width are now read from gui.config, if they are
  available, using keys "window_width" and "window_height".
  **ui.py, class ASEGUIWindow**::

    if config.__contains__('window_height') and \
                                    config['window_height'] != None:
        winheight = config['window_height']
    else:
        winheight = 450
    if config.__contains__('window_width') and \
                                    config['window_width'] != None:
        winwidth = config['window_width']
    else:
        winwidth = 450
    self.size = np.array([winwidth, winheight])
  
  Default behaviour is the same as before: width and height set to 450


- The 'Preferences' menu has new options for changing default behaviour

  - The user can now choose to use the ``Delete`` key for deleting
    things.
  - The user can choose to suppress the 'Movie' and 'Graphs' windows
    which open automatically by default when viewing more than one 
    image.
  - Since the typical 'Redo' shortcut ``Ctrl+Y`` is already used
    by ASE, the user is given the option to switch it to redo. In this
    case, 'Modify' will simply not have a shortcut.


- Pressing ``Alt+Y`` should now do the same as 
  'View' > 'Change View' > 'xz-plane', which it previously did not.