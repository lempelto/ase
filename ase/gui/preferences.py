from functools import partial
from subprocess import call
import configparser
import os.path
import os
import sys
from weakref import KeyedRef
from ase.gui.i18n import _
import ase.gui.ui as ui
import ase
import numpy as np

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.colorchooser

if not ttk:
    ttk = tk


# More user-friendly descriptions to be shown in 'Preferences' menu
pretty_names = {
    'gui_graphs_string': 'Default graph string',
    'max_history': 'Maximum undo history',
    'gui_foreground_color': 'Foreground color',
    'gui_background_color': 'Background color',
    'gui_scaling': 'GUI scaling',
    'covalent_radii': 'Covalent radii',
    'radii_scale': 'Atomic radii scaling',
    'force_vector_scale': 'Force vector scaling',
    'velocity_vector_scale': 'Velocity vector scaling',
    'show_unit_cell': 'Show unit cell',
    'show_axes': 'Show axes',
    'show_bonds': 'Show bonds',
    'shift_cell': 'Shift cell',
    'swap_mouse': 'Swap L/R mouse buttons',
    'swap_delete': "Delete using 'Delete' key",
    'enable_redokey': "Use 'Ctrl+Y' for redo",
    'auto_movie': "Auto-show 'Movie' window",
    'auto_graphs': "Auto-show 'Graphs' window",
    'window_height': 'Main window height',
    'window_width': 'Main window width',
}
# configparser unfortunately gets and saves everything as strings
# To use the values, we have to keep track of what to convert them to
value_types = {
    'gui_graphs_string': str,
    'max_history': int,
    'gui_foreground_color': str,
    'gui_background_color': str,
    'gui_scaling': float,
    'covalent_radii': float,
    'radii_scale': float,
    'force_vector_scale': float,
    'velocity_vector_scale': float,
    'show_unit_cell': bool,
    'show_axes': bool,
    'show_bonds': bool,
    'shift_cell': bool,
    'swap_mouse': bool,
    'swap_delete': bool,
    'enable_redokey': bool,
    'auto_movie': bool,
    'auto_graphs': bool,
    'window_height': int,
    'window_width': int,
}
# These values require a restart to be applied
needs_restart = [
    'gui_graphs_string',
    'gui_foreground_color',
    'gui_scaling',
    'covalent_radii',
    'show_bonds',
    'shift_cell',
    'swap_mouse',
    'swap_delete',
    'auto_movie',
    'auto_graphs'
]

gui_default_settings = {
    'gui_graphs_string': 'i, e - E[-1]',  # default for the graph command
    'max_history': 20,
    'gui_foreground_color': '#000000',
    'gui_background_color': '#ffffff',
    'gui_scaling': 1.,
    'covalent_radii': None,
    'radii_scale': 0.89,
    'force_vector_scale': 1.0,
    'velocity_vector_scale': 1.0,
    'show_unit_cell': True,
    'show_axes': True,
    'show_bonds': False,
    'shift_cell': False,
    'swap_mouse': False,
    'swap_delete': False,
    'enable_redokey': False,
    'auto_movie': True,
    'auto_graphs': True,
    'window_height': 450,
    'window_width': 500,
}


def read_preferences(iniFile='~/.config/ase/gui.ini'):
    """Return dictionary or None if the file doesn't exist"""
    if sys.platform == 'win32':
            iniFile, f = os.path.split(ase.__file__)
            iniFile += '\\gui.ini'
    
    config = {}

    iniFile = os.path.expanduser(iniFile)
    iniFile = os.path.abspath(iniFile)

    if os.path.exists(iniFile):
        parser = configparser.ConfigParser()
        parser.read(iniFile)
        for key in pretty_names:
            if key in parser['GUI']:
                try:
                    # Try to get config values as their matching types
                    if value_types[key] == str:
                        config[key] = parser['GUI'][key]
                    elif value_types[key] == int:
                        config[key] = parser['GUI'].getint(key)
                    elif value_types[key] == bool:
                        config[key] = parser['GUI'].getboolean(key)
                    # Try to get everything that runs over as floats
                    else:
                        config[key] = parser['GUI'].getfloat(key)

                except ValueError:
                    config[key] = None
                    # One possible bug: if something can't be retrieved as
                    # its type even though it should be, it may end up here
            else:
                config[key] = gui_default_settings[key]

        # Random test: is window width an integer
        if not isinstance(config['window_width'], int):
            # Configuration file is probably corrupted and should not be used
            config = gui_default_settings
        # Really, a more robust test should probably be implemented eventually
        # and/or this error should be handled with more nuance
    else:
        config = gui_default_settings
    
    return config

def write_preferences(config, iniFile='~/.config/ase/gui.ini'):

    iniFile = os.path.expanduser(iniFile)
    iniFile = os.path.abspath(iniFile)
    
    if sys.platform == 'win32':
        config_folder, f = os.path.split(ase.__file__)
        iniFile = config_folder + '\\gui.ini'
    else:
        config_folder, f = os.path.split(iniFile)

    if not os.path.exists(config_folder):
        os.makedirs(config_folder)

    parser = configparser.ConfigParser()
    parser.read_dict({'GUI':config})
    
    with open(iniFile, 'w') as file:
        parser.write(file)


class PrefColorPicker(ui.Widget):
    def __init__(self, master, initial, target) -> None:
        self.target = target
        self.value = initial
        self.picker = partial(tkinter.colorchooser.askcolor, initial)
        self.button = ttk.Button(master,text='RGB',command=self.ask,width=4)

    def ask(self):
        rgb, self.value = self.picker()
        if self.value != None:
            self.target.value = self.value


class Preferences:
    """Window for setting preferences/default properties for the ASE GUI"""
    def __init__(self, gui) -> None:
        self.gui = gui

        win = self.win = ui.Window(_('Preferences'), wmtype='normal')
        win.win.lift()
        win.win.focus()
        handle = self.handle = ui.DockWindow(gui.dock, _('Preferences'), close=win.close)
        liftbutton = ui.Button(_('Bring to front'), win.win.lift)
        liftbutton.grid(handle.win, column=1,row=0)
        gui.dock.update()

        win.win.columnconfigure(0,weight=1)
        win.win.rowconfigure(0,weight=1)

        contents = tk.Frame(win.win)
        contents.grid(column=0,row=0,padx=(20,0),pady=10,sticky=tk.NSEW)
        contents.columnconfigure(0,weight=1)
        contents.columnconfigure(1,weight=2)
        contents.rowconfigure(1,weight=1)

        head = ui.Label(_('Set default values to use when opening ASE')).create(contents)
        head.config(font='TkHeadingFont')
        head.grid(column=0,columnspan=2,row=0,pady=(0,5),sticky=tk.W)
        

        cframe = ui.ScrollFrame(maxview=450)
        cframe.grid(contents,column=0,columnspan=2,row=1,sticky=tk.NSEW,pady=(5,10))
        cframe.columnconfigure(0,weight=2)
        cframe.columnconfigure(1,weight=3)

        # It would be good to not hard-code this.
        # This should be changed somewhere down the line
        iniFile = '~/.config/ase/gui.ini'
        iniFile = os.path.expanduser(iniFile)
        iniFile = os.path.abspath(iniFile)
        
        conf_file_exists = False
        if os.path.exists(iniFile):
            conf_file_exists = True

        config = self.gui.config
        self.old_config = config.copy()
        self.in_config = {}


        i = 0
        for key in pretty_names:
            pretty_key = pretty_names[key]
            _key = ui.Label(_(pretty_key))
            
            if key in config:
                value = config[key]
                if value == None: value = ''
            else:
                value = ''

            if value_types[key] == bool:
                if value == '': value = False
                _value = ui.CheckButton('',value=value,callback=self.get_input)
            else:
                _value = ui.Entry(value,callback=self.get_input)

            self.in_config[key] = _value

            _key.create(cframe).grid(column=0,
                                     row=i,
                                     sticky=tk.E)
            _value.create(cframe).grid(column=1,
                                       row=i,
                                       sticky=tk.EW)

            if key in ['gui_foreground_color', 'gui_background_color']:
                picker = PrefColorPicker(cframe, value, _value)
                picker.button.grid(column=2,row=i)
            
            i += 1
        cframe.update()

        save_btn = ttk.Button(contents, text=_('Apply'),
                              command=self.save_preferences)
        save_btn.grid(column=0,row=2,sticky=tk.EW,padx=(0,10))
        
        if conf_file_exists:
            does_exist_label = ui.Label(_('(Configuration file will be overwritten)'),
                                        color='#707070')
            does_exist_label.create(contents).grid(column=0,
                                                   row=3,
                                                   sticky=tk.W)
            self.existancelabel = does_exist_label
        else:
            does_not_exist_label = ui.Label(
                                _('Configuration file does not yet exist'),
                                color='#707070')
            does_not_exist_label.create(contents).grid(column=0,
                                                       columnspan=2,
                                                       row=3)
            self.existancelabel = does_not_exist_label

        self.notelabel = ui.Label(' ')
        self.notelabel.create(contents).grid(column=0,columnspan=2,row=4)

        win.win.bind(
            '<Destroy>',
            lambda e: handle.close())

    def get_input(self):
        old_config = self.old_config
        for key in pretty_names:
            try:
                if value_types[key] == str:
                    value = str(self.in_config[key].value)
                elif value_types[key] == int:
                    value = int(self.in_config[key].value)
                elif value_types[key] == bool:
                    value = bool(self.in_config[key].value)
                else:
                    value = float(self.in_config[key].value)
            except ValueError:
                value = None

            self.gui.config[key] = value

            try:
                old_val = old_config[key]
            except KeyError:
                old_val = None
            if (value != old_val) and key in needs_restart:
                self.notelabel.text = _('Some changes will require a restart')


    def apply_preferences(self):
        self.get_input()
        config = self.gui.config

        self.gui.maxhistory = config['max_history']

        self.gui.window['toggle-show-unit-cell'] = config['show_unit_cell']
        self.gui.toggle_show_unit_cell()
        self.gui.window['toggle-show-axes'] = config['show_axes']
        self.gui.toggle_show_axes()
        self.gui.window['toggle-show-bonds'] = config['show_bonds']
        self.gui.toggle_show_bonds()

        self.gui.window.fg = ['gui_foreground_color']
        self.gui.window.bg = ['gui_background_color']
        self.gui.window.canvas.size = np.array([config['window_width'], 
                                                config['window_height']])
        self.gui.window.canvas.configure(width=config['window_width'], 
                                         height=config['window_height'], 
                                         bg=config['gui_background_color'])
        
        self.gui.force_vector_scale = config['force_vector_scale']
        self.gui.velocity_vector_scale = config['velocity_vector_scale']
        self.gui.images.atom_scale = config['radii_scale']
        self.gui.draw()


    def save_preferences(self, iniFile='~/.config/ase/gui.ini'):
        """save into .ini config file"""
        self.apply_preferences()
        
        config = self.gui.config.copy()
        for key in config:
            config[key] = str(config[key])
        
        write_preferences(config=config, iniFile=iniFile)
        
        self.existancelabel.text = _("Preferences saved")
